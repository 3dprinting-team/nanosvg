Source: nanosvg
Section: libs
Priority: optional
Maintainer: Chow Loong Jin <hyperair@debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               cmake,
Standards-Version: 4.6.2
Homepage: https://github.com/memononen/nanosvg
Vcs-Browser: https://salsa.debian.org/3dprinting-team/nanosvg
Vcs-Git: https://salsa.debian.org/3dprinting-team/nanosvg.git

Package: libnanosvg-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
Description: simple svg parsing library -- development files
 NanoSVG is a simple stupid single-header-file SVG parse. The output of
 the parser is a list of cubic bezier shapes.
 .
 The library suits well for anything from rendering scalable icons in
 your editor application to prototyping a game.
 .
 NanoSVG supports a wide range of SVG features, but something may be
 missing, feel free to create a pull request!
 .
 The shapes in the SVG images are transformed by the viewBox and
 converted to specified units. That is, you should get the same looking
 data as your designed in your favorite app.
 .
 NanoSVG can return the paths in few different units. For example if you
 want to render an image, you may choose to get the paths in pixels, or
 if you are feeding the data into a CNC-cutter, you may want to use
 millimeters.
 .
 The units passed to NanoSVG should be one of: 'px', 'pt', 'pc' 'mm',
 'cm', or 'in'. DPI (dots-per-inch) controls how the unit conversion is
 done.
 .
 If you don't know or care about the units stuff, "px" and 96 should get
 you going.
 .
 This package contains header files, other development files, and a static
 library required to compile programs against this library.
